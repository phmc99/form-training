import { FormBox, TextField, Label, Button, Error } from "./style";

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useHistory } from "react-router-dom";

const Form = () => {
  const history = useHistory();

  const handleClick = () => {};

  const formSchema = yup.object().shape({
    nome: yup.string().required("Nome obrigatório"),
    email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
    senha: yup
      .string()
      .required("Senha obrigatório")
      .matches(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
        "Senha com no mínimo 8 caracteres. Necessário ter letras, números e ao menos um símbolo."
      ),
    confirmaSenha: yup
      .string()
      .oneOf([yup.ref("senha"), null], "As senhas não são iguais"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmitFunction = (data) => {
    console.log(data);
    history.push("/welcome");
  };

  return (
    <FormBox>
      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <Label htmlFor="nome">Nome</Label>
        <TextField
          required
          name="nome"
          type="text"
          placeholder="Nome"
          {...register("nome")}
        />
        {errors.nome && <Error>{errors.nome?.message}</Error>}

        <Label htmlFor="email">E-mail</Label>
        <TextField
          required
          name="email"
          type="email"
          placeholder="E-mail"
          {...register("email")}
        />
        {errors.email && <Error>{errors.email?.message}</Error>}

        <Label htmlFor="senha">Senha</Label>
        <TextField
          required
          name="senha"
          type="password"
          placeholder="Senha"
          {...register("senha")}
        />
        {errors.senha && <Error>{errors.senha?.message}</Error>}

        <Label htmlFor="confirmaSenha">Confirmar senha</Label>
        <TextField
          required
          name="confirmaSenha"
          type="password"
          placeholder="Confirmar Senha"
          {...register("confirmaSenha")}
        />
        {errors.confirmaSenha && <Error>{errors.confirmaSenha?.message}</Error>}

        <Label htmlFor="telefone">Telefone</Label>
        <TextField name="telefone" type="tel" />

        <Label htmlFor="linkedin">Linkedin</Label>
        <TextField name="linkedin" type="url" />

        <Label htmlFor="gitHub">GitHub</Label>
        <TextField name="gitHub" type="url" />
        <Button onClick={handleClick} type="submit">
          Enviar
        </Button>
      </form>
    </FormBox>
  );
};

export default Form;
