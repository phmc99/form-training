import styled from "styled-components";

export const FormBox = styled.div`
  width: 300px;
  box-shadow: 2px 2px 5px gray;
  border-radius: 5px;
  background-color: #f3f3f3;
  padding: 10px;

  form {
    display: flex;
    flex-direction: column;
  }

  @media (min-width: 768px) {
    width: 400px;
  }
`;

export const TextField = styled.input`
  border: 0;
  width: 90%;
  padding: 10px;
  border-radius: 5px;
  background-color: lightgray;
`;

export const Label = styled.label`
  font-size: 12px;
  color: rgb(55, 55, 55);
  margin-top: 10px;

  @media (min-width: 768px) {
    font-size: 14px;
  }
`;

export const Button = styled.button`
  width: 80px;
  border: 0;
  background-color: royalblue;
  text-transform: uppercase;
  font-variant: small-caps;
  color: #f3f3f3;
  font-weight: 600;
  padding: 10px;
  border-radius: 5px;
  margin-top: 10px;
  align-self: center;
`;

export const Error = styled.span`
  color: darkred;
  font-size: 12px;
`;
