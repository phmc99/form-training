import Form from "../../components/Form";
import "./style.css";

const Home = () => {
  return (
    <main>
      <div className="content">
        <h1>Formulario</h1>
        <Form />
      </div>
    </main>
  );
};

export default Home;
